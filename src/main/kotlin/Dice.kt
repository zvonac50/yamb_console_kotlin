import kotlin.random.Random

class Dice(id: Int) {
    private val ID: Int = id
    val ExtraDice = false
    private var IsLocked = false
    var RolledNumber: Int = 0

    fun rollDice() {
        if (!this.IsLocked)
            this.RolledNumber = Random.nextInt(1, 7)
    }

    fun changeLockedState() {
        this.IsLocked = !IsLocked
    }

    override fun toString(): String {
        return "Dice ID: $ID\nIs this Dice aside: $IsLocked\nNumber rolled: $RolledNumber\n-----------------------------"
    }
}