class GameController(diceWorker: DiceWorker) {
    var currentRound: Int = 0
    private val diceWorkerRef: DiceWorker = diceWorker
    var totalScore: Int = 0

    fun checkForBonuses() {
        val allRolledValues: List<Int> = this.diceWorkerRef.getListOfDiceValues().sorted()
        println(allRolledValues)

        checkForJAMBPoints(allRolledValues)
        checkForPOKERPoints(allRolledValues)
        checkForKENTAPoints(allRolledValues)
    }

    fun getDicesSum(): Int {
        var sumOfDices: Int = 0
        for (dice in diceWorkerRef.getDicesBox()) {
            sumOfDices += dice.value.RolledNumber
        }
        return sumOfDices
    }

    private fun checkForKENTAPoints(values: List<Int>) {
        var sequenceCounter: Int = 0
        val distinctValues = values.distinct()
        for (i in 0..distinctValues.size - 2) {
            if (sequenceCounter == 5) {
                break
            }
            if (distinctValues[i] == (distinctValues[i + 1] - 1)) {
                sequenceCounter++
            } else {
                sequenceCounter = 0
            }
        }

        if (sequenceCounter > 0) sequenceCounter++
        if (sequenceCounter >= 5) {
            when (this.currentRound) {
                1 -> this.totalScore += 66
                2 -> this.totalScore += 56
                else -> {
                    this.totalScore += 46
                }
            }
            println("You got extra points for KENTA!")
        }
    }

    private fun checkForPOKERPoints(values: List<Int>) {
        if (values.groupingBy { it }.eachCount().any { it.value == 4 }) {
            println("You got extra points for POKER!")
            this.totalScore += (values.groupingBy { it }.eachCount()
                .filter { it.value == 4 }.keys.toIntArray()[0]) * 4 + 40
        }
    }

    private fun checkForJAMBPoints(values: List<Int>) {
        if (values.groupingBy { it }.eachCount().any { it.value == 5 }) {
            println("You got extra points for JAMB!")
            this.totalScore += (values.groupingBy { it }.eachCount()
                .filter { it.value == 5 }.keys.toIntArray()[0]) * 5 + 50
        }
    }
}